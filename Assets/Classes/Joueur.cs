﻿using System;

namespace Assets.Classes
{
	[Serializable]
	class Joueur
	{
		public int ID { get; set; }
		public string Nom { get; private set; }
		public int Score { get; set; }
		public bool Pret { get; set; }
		public int NbrCartes { get; set; }
		public bool Czar { get; set; }

		public Joueur(string nom, int id = -1)
		{
			ID = id;
			Nom = nom;
			Pret = false;
			Score = 0;
			NbrCartes = 0;
			Czar = false;
		}
	}
}
