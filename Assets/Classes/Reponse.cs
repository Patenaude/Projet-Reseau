﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Classes
{
	[Serializable]
    class Reponse
    {
		public int ID { get; set; }
		public int IDJoueur { get; private set; }
		public List<int> IDCartesBlanches { get; private set; }
		public int IDCarteNoire { get; private set; }

		public Reponse(int idjoueur, int[] cb, int cn)
        {
			ID = -1;
            IDJoueur = idjoueur;
			IDCarteNoire = cn;
        }
    }
}
