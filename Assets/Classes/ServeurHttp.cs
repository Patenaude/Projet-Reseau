﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using SimpleHttpServer;
using SimpleHttpServer.Models;
using UnityEngine;
using Assets.Classes;
using System.Web;

public class ServeurHttp : MonoBehaviour {

	// Use this for initialization         
	void Start () {
		var route_config = new List<Route>()
		{
			new Route
			{
				Name = "Handler",
				UrlRegex = @"",
				Method = "GET",
				Callable = (SimpleHttpServer.Models.HttpRequest request) => 
				{
					string id = HttpUtility.ParseQueryString(request.Url).Get("id");
					string key = HttpUtility.ParseQueryString(request.Url).Get("k");
					int r = DAOJoueurs.Confirm(int.Parse(id), key);

					if (r == 1)
					{
						return new SimpleHttpServer.Models.HttpResponse()
						{
							ContentAsUTF8 =  "Compte confirmé", 
							ReasonPhrase = "Confirmé",
							StatusCode = "200"
						};
					}
					else if(r == -1)
					{
						return new SimpleHttpServer.Models.HttpResponse()
						{
							ContentAsUTF8 =  "Lien invalide",
							ReasonPhrase = "Invalide",
							StatusCode = "200"
						};
					}
					else if(r == -2)
					{
						return new SimpleHttpServer.Models.HttpResponse()
						{
							ContentAsUTF8 =  "Erreur",
							ReasonPhrase = "Erreur",
							StatusCode = "200"
						};
					}
					else
					{
						return new SimpleHttpServer.Models.HttpResponse()
						{
							ContentAsUTF8 =  "Erreur",
							ReasonPhrase = "Erreur",
							StatusCode = "200"
						};
					}
				}
			}
        };

		HttpServer httpServer = new HttpServer(9999, route_config);

		Thread thread = new Thread(new ThreadStart(httpServer.Listen));
		thread.Start();
	}
}
