﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using Assets.Classes;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

public class NetworkServer : MonoBehaviour {

	#region variables
	public Text console;

	int port = 11111;
	public int maxConnections = 10;
	public string separateur = "/~/";

	int serverSocket;
	int serverWebSocket;
	byte channelID;

	Partie partie = null;
	System.Random random = new System.Random();
	Dictionary<int, int> clients = new Dictionary<int, int>(); //connection id, player id
	Dictionary<int, int> clientsweb = new Dictionary<int, int>(); //connection id, player id
	Dictionary<int, int> clientstemp = new Dictionary<int, int>(); //connection id, socket id
	#endregion

	void Start()
	{
		DontDestroyOnLoad(this);
		Log("Server Started");

		GlobalConfig gc = new GlobalConfig();
		gc.ReactorModel = ReactorModel.FixRateReactor;
		gc.ThreadAwakeTimeout = 10;
		gc.MaxPacketSize = 20000;
		ConnectionConfig cc = new ConnectionConfig();
		channelID = cc.AddChannel(QosType.ReliableFragmented);
		HostTopology ht = new HostTopology(cc, maxConnections);
		NetworkTransport.Init(gc);

		serverSocket = NetworkTransport.AddHost(ht, port);
		serverWebSocket = NetworkTransport.AddWebsocketHost(ht, port);

		if (serverSocket < 0) { Log("Server socket creation failed!"); }
		else { Log("Socket opened on port " + port + ". ID : " + serverSocket); }
		if (serverWebSocket < 0) { Log("Server websocket creation failed!"); }
		else { Log("Websocket opened on port " + port + ". ID : " + serverWebSocket); }
	}

	void Update()
	{
		int recHostId;
		int connectionId;
		int channelId;
		int dataSize;
		byte[] buffer = new byte[20480];
		byte error;

		NetworkEventType networkEvent = NetworkEventType.DataEvent;

		do
		{
			networkEvent = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, buffer, 20480, out dataSize, out error);

			if (networkEvent == NetworkEventType.Nothing) { }
			else if (networkEvent == NetworkEventType.ConnectEvent)
			{
				Log("Player " + connectionId.ToString() + " connected!");
				clientstemp.Add(connectionId, recHostId);
			}
			else if (networkEvent == NetworkEventType.DataEvent)
			{
				Stream stream = new MemoryStream(buffer);
				BinaryFormatter f = new BinaryFormatter();
				string msg = f.Deserialize(stream).ToString();
				Log("Received Data from " + connectionId.ToString() + "! Message: " + msg);
				TraiterMessage(msg, connectionId, recHostId);
			}
			else if (networkEvent == NetworkEventType.DisconnectEvent)
			{
				Log("Received disconnect from " + connectionId.ToString());
			}
		} while (networkEvent != NetworkEventType.Nothing);
	}

	private void StartTimerPret()
	{
		//throw new NotImplementedException();
	}
	private void StartPartie(object sender, EventArgs e)
	{
		SendPartieStarted();
		SendCartesAll();
		SendCarteNoire();
		SendCzar();
	}

	void TraiterMessage(string message, int cid, int socketid)
	{
		string[] parametres = message.Split(new string[] { separateur }, StringSplitOptions.RemoveEmptyEntries);

		if (parametres[0] == "InfoJoueur")
		{
			int pid = GetIDs(cid, socketid).Value.Value;
			Joueur joueur = (Joueur)DeserializeBinary(parametres[1]);
			
			if (!partie.AjouterJoueur(joueur))
			{
				SendData("Erreur" + separateur + "joueur avec id " + pid + " deja dans partie.", pid);
				Log("Erreur! joueur avec id " + pid + "deja dans partie.");
				return;
			}
			else
			{
				Log("Ajouter joueur " + pid + ". Nom : " + joueur.Nom + "; ID : " + joueur.ID);
				SendNewJoueur(joueur);
			}
		}
		else if (parametres[0] == "Pret")
		{
			int pid = GetIDs(cid, socketid).Value.Value;

			SendPret(pid, parametres[1]);

			bool pret = (parametres[1] == "True") ? true : false;
			bool? x = partie.SetPret(pid, pret);
			if (x == false)
			{
				SendData("Erreur" + separateur + "joueur avec id " + pid + "pas trouve dans la partie.", pid);
				Log("Erreur! joueur avec id " + pid + "pas trouve dans la partie.");
				return;
			}
			else
			{
				
				Log("Joueur" + pid + " pret");
				if (x == true)
				{
					StartTimerPret();
				}
			}
		}
		else if (parametres[0] == "Reponse")
		{
			int pid = GetIDs(cid, socketid).Value.Value;

			if (partie.Started == false || partie.CarteNoire == null)
			{
				SendData("Erreur" + separateur + "partie non debutee ou sant attente de reponses", pid);
				Log("Erreur! partie non debutee ou sant attente de reponses");
				return;
			}
			else
			{
				Reponse reponse = (Reponse)(DeserializeBinary(parametres[1]));
				int[] cbs = reponse.IDCartesBlanches.ToArray<int>();
				

				bool? x = partie.AjouterReponse(reponse);
				if (x == false)
				{
					SendData("Erreur" + separateur + "partie contient deja une reponse pour joueur " + pid, pid);
					Log("Erreur! partie contien deja une reponse pour joueur " + pid);
				}
				else if (x == true)
				{
					SendReponses();	
				}
				else if (x == null)
				{
					Log("reponse du joueur " + pid + " ajoute");
				}
			}
		}
		else if (parametres[0] == "Choix")
		{
			int pid = GetIDs(cid, socketid).Value.Value;

			if (partie.Reponses != null)
			{
				int rid = Int16.Parse(parametres[1]);
				Reponse reponse = null;
				foreach (Reponse r in partie.Reponses)
				{
					if (r.ID == rid)
					{
						reponse = r;
						break;
					}
				}

				if (reponse == null)
				{
					SendData("Erreur" + separateur + "aucune reponse ne correspond a l'id donne", pid);
					Log("Erreur! aucune reponse ne correspond a l'id donne");
					return;
				}
				else
				{
					SendDataAll(message);
					partie.GetJoueurWithID(reponse.IDJoueur).Score += 1;
					partie.NextTour();
					SendInfoJoueursAll();
					SendCartesAll();
					SendCarteNoire();
					SendCzar();
					//TODO revoir
				}
			}
			else
			{
				SendData("Erreur" + separateur + "impossible d'accepter un choix a ce moment", pid);
				Log("Erreur! impossible d'accepter un choix a ce moment");
				return;
			}
		}
		else if (parametres[0] == "GetInfoJoueurs")
		{
			int pid = GetIDs(cid, socketid).Value.Value;

			SendInfoJoueurs(pid);
			Log("Enoye info joueurs a joueur " + pid);
		}
		else if (parametres[0] == "Message")
		{
			SendMessage(parametres[1]);
		}
		else if (parametres[0] == "Inscrire")
		{
			string email = parametres[1];
			string mdp = parametres[2];
			string nom = parametres[3];
			if (!DAOJoueurs.EmailExiste(email).Value)
			{
				//TODO valider mot de passe?
				string key = GetKey();
				bool r = DAOJoueurs.InsertJoueurs(email, mdp, nom, key);
				if (r)
				{
					SendEmailConfirmation(email, nom, key, DAOJoueurs.GetID(email));
					SendInscrire(cid, socketid, 1);
				}
				else
					SendInscrire(cid, socketid, -1);
			}
			else
			{
				SendInscrire(cid, socketid, 2);
			}
		}
		else if (parametres[0] == "Connecter")
		{
			string email = parametres[1];
			string mdp = parametres[2];
			int? r = DAOJoueurs.Valide(email, mdp);
			if(r >= 0)
			{
				if (!IsConnected((int)r))
				{
					string nom = DAOJoueurs.GetNom((int)r);
					SendConnecter(cid, socketid, (int)r, nom);
					AjouterJoueur((int)r, nom, cid, socketid);
				}
				else
					SendConnecter(cid, socketid, 5);
			}
			else if(r == -1)
				SendConnecter(cid, socketid, 2);
			else if (r == -2)
				SendConnecter(cid, socketid, 3);
			else if (r == -3)
				SendConnecter(cid, socketid, 4);
			else if (r == -4)
				SendConnecter(cid, socketid, -1);
		}
	}

	#region send
	void SendPartieStarted()
	{
		SendDataAll("PartieStarted");
	}
	void SendCartesAll()
	{
		foreach(Joueur j in partie.Joueurs)
		{
			if(j.ID != -1 && j.NbrCartes < 10)
			{
				int[] cartes = new int[10 - j.NbrCartes];
				int nbc = 0;
				for(int x = 0; x < 10 - j.NbrCartes; x++)
				{
					cartes[x] = partie.GetCarteBlanche();
					nbc++;
				}
				j.NbrCartes += nbc;
				string msg = "CartesBlanches" + separateur + nbc.ToString();
				foreach (int c in cartes)
					msg += separateur + c.ToString();
				SendData(msg, j.ID);
			}
		}
	}
	void SendCartes(int pid)
	{
		Joueur j = partie.GetJoueurWithID(pid);
		if (j.ID != -1 && j.NbrCartes < 10)
		{
			int[] cartes = new int[10 - j.NbrCartes];
			for (int x = 0; x < 10 - j.NbrCartes; x++)
			{
				cartes[x] = partie.GetCarteBlanche();
			}
			string msg = "CartesBlanches" + separateur + (10 - j.NbrCartes).ToString();
			foreach (int c in cartes)
				msg += separateur + c.ToString();
			SendData(msg, pid);
		}
	}
	void SendCarteNoire()
	{
		SendDataAll("CarteNoire" + separateur + partie.CarteNoire.ToString());
	}
	void SendCzar()
	{
		SendDataAll("Czar" + separateur + partie.Joueurs[partie.Czar].ID);
	}
	void SendReponses()
	{
		string data = "Reponses" + separateur + partie.NbReponses;
		for (int i = 0; i < partie.NbReponses; i++)
		{
			Reponse r = partie.Reponses[i];
			data += separateur + r.ID;
			for(int x = 0; x < CarteNoire.CartesNoires[partie.Reponses[i].IDCarteNoire].NbCartes; x++)
			{
				data += separateur + r.IDCartesBlanches[x];
			}
		}
		SendDataAll(data);
	}
	void SendInfoJoueurs(int pid)
	{
		string data = "InfoJoueurs" + separateur + partie.NbJoueurs + separateur;
		for (int x = 0; x < partie.NbJoueurs; x++)
		{
			data += SerializeBinary(partie.Joueurs[x]);
			if (x != partie.NbJoueurs)
				data += separateur;
		}
		SendData(data, pid);
	}
	void SendInfoJoueursAll()
	{
		string data = "InfoJoueurs" + separateur + partie.NbJoueurs + separateur;
		for (int x = 0; x < partie.NbJoueurs; x++)
		{
			data += SerializeBinary(partie.Joueurs[x]);
			if (x != partie.NbJoueurs)
				data += separateur;
		}
		SendDataAll(data);
	}
	new void SendMessage(string message)
	{
		SendDataAll("Message" + separateur + message);
	}
	void SendPret(int pidPret, string pret)
	{
		SendDataAll("Pret" + separateur + pidPret.ToString() + separateur + pret);
	}
	void SendErreur(int id, string erreur)
	{
		Log("Erreur : " + erreur);
		SendData("Erreur" + separateur + erreur, id);
	}
	void SendNewJoueur(Joueur j)
	{
		string msg = "NewJoueur" + separateur + SerializeBinary(j);
		SendDataAllExcept(msg, j.ID);
	}
	void SendID(int pid)
	{
		Log("envoie ID au joueur " + pid);
		SendData("ID" + separateur + pid, pid);
	}
	void SendConnecter(int cid, int socketid, int value)
	{
		Log("envoie Connecter " + value + " au joueur " + cid);
		SendData("Connecter" + separateur + value, cid, socketid);
	}
	void SendConnecter(int cid, int socketid, int id, string nom)
	{
		Log("envoie Connecter 1 au joueur " + cid);
		SendData("Connecter" + separateur + 1 + separateur + id + separateur + nom, cid, socketid);
	}
	void SendInscrire(int cid, int socketid, int value)
	{
		Log("envoie Inscrire " + value + " au joueur " + cid);
		SendData("Inscrire" + separateur + value, cid, socketid);
	}

	void SendData(string message, int pid)
	{
		KeyValuePair<int, int>? ids = GetCIDSID(pid);
		SendData(message, ids.Value.Key, ids.Value.Value);
	}
	void SendData(string message, int cid, int socketid)
	{
		byte error;
		byte[] buffer = new byte[20480];
		Stream stream = new MemoryStream(buffer);
		BinaryFormatter f = new BinaryFormatter();
		f.Serialize(stream, message);

		NetworkTransport.Send(socketid, cid, channelID, buffer, (int)stream.Position, out error);

		Log("Message \"" + message + "\" envoyé au joueur " + cid);
	}
	void SendDataAll(string message)
	{
		byte error;
		byte[] buffer = new byte[20480];
		Stream stream = new MemoryStream(buffer);
		BinaryFormatter f = new BinaryFormatter();
		f.Serialize(stream, message);

		int x = 0;
		foreach (Joueur j in partie.Joueurs)
		{
			KeyValuePair<int, int>? ids = GetCIDSID(j.ID);
			NetworkTransport.Send(ids.Value.Value, ids.Value.Key, channelID, buffer, (int)stream.Position, out error);
			x++;
		}

		Log("Message \"" + message + "\" enoyé a " + x + "joueurs");
	}
	void SendDataAllExcept(string message, int pid)
	{
		byte error;
		byte[] buffer = new byte[20480];
		Stream stream = new MemoryStream(buffer);
		BinaryFormatter f = new BinaryFormatter();
		f.Serialize(stream, message);

		int x = 0;
		foreach (Joueur j in partie.Joueurs)
		{
			if (j.ID != pid)
			{
				KeyValuePair<int, int>? ids = GetCIDSID(j.ID);
				NetworkTransport.Send(ids.Value.Value, ids.Value.Key, channelID, buffer, (int)stream.Position, out error);
				x++;
			}
		}

		Log("Message \"" + message + "\" enoyé a " + x + "joueurs");
	}
	#endregion

	public void Log(string message)
	{
		console.text = message + "\n";
	}

	public string SerializeBinary(object o)
	{
		using (MemoryStream ms = new MemoryStream())
		{
			new BinaryFormatter().Serialize(ms, o);
			return Convert.ToBase64String(ms.ToArray());
		}
	}
	public object DeserializeBinary(string s)
	{
		byte[] bytes = Convert.FromBase64String(s);
		using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
		{
			ms.Write(bytes, 0, bytes.Length);
			ms.Position = 0;
			return new BinaryFormatter().Deserialize(ms);
		}
	}

	//public string GetHash(Object o)
	//{

	//}

	public string GetKey()
	{
		byte[] key = new byte[16];
		RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
		rng.GetBytes(key);
		return BitConverter.ToString(key);
	}

	/// <summary>
	/// prend le connection id et le socket id et retourne le connectionid et player id
	///	du joueur
	/// </summary>
	/// <returns>
	/// connection id et player id du joueur avec la connection id
	/// null si not found
	/// </returns>
	KeyValuePair<int, int>? GetIDs(int connectionid, int socketid)
	{
		int pid;
		if (socketid == serverSocket)
			if (clients.TryGetValue(connectionid, out pid))
				return new KeyValuePair<int, int>(connectionid, pid);
			else
				return null;
		else if(socketid == serverWebSocket)
			if (clientsweb.TryGetValue(connectionid, out pid))
				return new KeyValuePair<int, int>(connectionid, pid);
			else
				return null;
		return null;
	}
	/// <summary>
	/// prend le player id et retourne le connection id et le socket id
	/// </summary>
	/// <returns>
	/// le connection id et socket id du joueur
	/// </returns>
	KeyValuePair<int, int>? GetCIDSID(int pid)
	{
		foreach (KeyValuePair<int, int> ids in clients)
		{
			if (ids.Value == pid)
				return new KeyValuePair<int, int>(ids.Key, serverSocket);
		}
		foreach (KeyValuePair<int, int> ids in clientsweb)
		{
			if (ids.Value == pid)
				return new KeyValuePair<int, int>(ids.Key, serverWebSocket);
		}
		return null;
	}

	void AjouterJoueur(int pid, string nom, int cid, int socketid)
	{
		if (partie == null)
		{
			partie = new Partie(1);
			partie.StartPartieHandler += StartPartie;
		}
		if (socketid == serverSocket)
			clients.Add(cid, pid);
		else if (socketid == serverWebSocket)
			clientsweb.Add(cid, pid);
		Joueur j = new Joueur(nom, pid);
		partie.AjouterJoueur(j);
		SendNewJoueur(j);
	}

	void SendEmailConfirmation(string email, string nom, string key, int id)
	{
		var fromAddress = new MailAddress("cclhemail@gmail.com", "Carte contre l'humanité");
		var toAddress = new MailAddress(email, nom);
		string fromPassword = "cclhemail1";
		string subject = "Confirmation compte CCLH";
		string body = "Veuillez confirmer votre compte à l'adresse suivante : 127.0.0.1:9999?id=" + id + "&k=" + key;

		SmtpClient smtpc = new SmtpClient()
		{
			Host = "smtp.gmail.com",
			Port = 587,
			EnableSsl = true,
			DeliveryMethod = SmtpDeliveryMethod.Network,
			UseDefaultCredentials = false,
			Credentials = (ICredentialsByHost)new NetworkCredential(fromAddress.Address, fromPassword)
		};
		ServicePointManager.ServerCertificateValidationCallback =
			delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
			{ return true; };

		MailMessage message = new MailMessage(fromAddress, toAddress);
		message.Subject = subject;
		message.Body = body;

		smtpc.Send(message);
	}

	bool IsConnected(int pid)
	{
		foreach(KeyValuePair<int, int> ids in clients)
		{
			if (ids.Value == pid)
				return true;
		}
		foreach (KeyValuePair<int, int> ids in clientsweb)
		{
			if (ids.Value == pid)
				return true;
		}
		return false;
	}
}