﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace Assets.Classes
{
	class Partie
    {
		public event EventHandler StartPartieHandler;

		public int ID { get; private set; }
        public List<Joueur> Joueurs { get; set; }
		public int NbJoueurs { get; set; }
		public int NbJoueursPret { get; set; }
        public bool Started { get; set; }
        public int NbRounds { get; private set; }
        public int NbTour { get; private set; }
		public List<Reponse> Reponses { get; set; }
		public int NbReponses { get; set; }
		public int? CarteNoire { get; set; }
		public Queue<int> ListeCartesBlanches { get; private set; }
		public Queue<int> ListeCartesNoires { get; private set; }
		public int Czar { get; private set; }
		public Timer TimerPret { get; private set; }
		public bool TimerPretStarted { get; private set; }

		System.Random random = new System.Random();

		public Partie(int id, int maxjoueurs = 10)
        {
			ID = id;
			Joueurs = new List<Joueur>(maxjoueurs);
			NbJoueurs = 0;
            Started = false;
			NbRounds = 0;
			NbTour = 0;
			Reponses = null;
			NbReponses = 0;
			CarteNoire = null;
			Czar = -1;
			TimerPret = null;
        }

		public bool AjouterJoueur(Joueur joueur)
		{
			foreach (Joueur j in Joueurs)
			{
				if (j.ID != -1 && j.ID == joueur.ID)
				{
					return false;
				}
			}
			Joueurs.Add(joueur);
			NbJoueurs++;
			return true;
		}

		public bool? SetPret(int id, bool value)
		{
			bool found = false;
			foreach(Joueur j in Joueurs)
			{
				if (j.ID == id)
				{
					j.Pret = value;
					NbJoueursPret++;
					found = true;
				}
			}
			if(found)
			{
				if (NbJoueursPret == NbJoueurs)
				{
					//TODO revoir timer pcq Unity + threads = impossible
					//StartTimerPret(3);
					StartPartie(EventArgs.Empty);
					return true;
				}
				return null;
			}
			return false;
		}

		public bool? AjouterReponse(Reponse reponse)
		{
			if (Reponses == null)
				Reponses = new List<Reponse>(NbJoueurs);
			else
			{
				foreach (Reponse r in Reponses)
				{
					if (r.ID != -1 && r.IDJoueur == reponse.IDJoueur)
						return false;
				}
			}
			reponse.ID = NbReponses++;
			Reponses.Add(reponse);
			GetJoueurWithID(reponse.IDJoueur).NbrCartes -= reponse.IDCartesBlanches.Count;

			if (NbReponses == NbJoueurs - 1)
				return true;
			return null;
		}

		public Joueur GetJoueurWithID(int id)
		{
			foreach(Joueur j in Joueurs)
			{
				if (j.ID == id)
					return j;
			}
			return null;
		}

		public int GetCarteBlanche()
		{
			if (ListeCartesBlanches == null || ListeCartesBlanches.Count == 0)
				ListeCartesBlanches = GenerateRandomQueue();
			return ListeCartesBlanches.Dequeue();
		}
		public int GetCarteNoire()
		{
			if (ListeCartesNoires == null || ListeCartesNoires.Count == 0)
				ListeCartesNoires = GenerateRandomQueue(165, 0, 165);
			return ListeCartesNoires.Dequeue();
		}
		public void NextTour()
		{
			if (Czar == NbJoueurs - 1)
			{
				//TODO prochain round
				NbRounds += 1;
				NbTour = 0;
			}
			Reponses = null;
			NbReponses = 0;
			NbTour++;
			CarteNoire = GetCarteNoire();
			Czar = NbTour - 1;
		}

		public void StartTimerPret(float duration)
		{
			TimerPret = new Timer();
			TimerPret.Interval = duration * 1000;
			TimerPret.Elapsed += StartPartie;
			TimerPret.Start();
		}

		public void StartPartie(object sender, System.Timers.ElapsedEventArgs e)
		{
			StartPartie(EventArgs.Empty);
		}
		public void StartPartie(EventArgs e)
		{
			Started = true;
			NbTour = 1;
			Czar = 0;
			CarteNoire = GetCarteNoire();
			StartPartieHandler(this, e);
		}

		public List<int> GenerateRandomList(int count = 671, int min = 0, int max = 671)
		{
			if (max <= min || count < 0 || (count > max - min && max - min > 0))
			{
				throw new ArgumentOutOfRangeException("Range " + min + " to " + max +
						" (" + ((Int64)max - (Int64)min) + " values), or count " + count + " is illegal");
			}

			HashSet<int> candidates = new HashSet<int>();

			for (int top = max - count; top < max; top++)
			{
				if (!candidates.Add(random.Next(min, top + 1)))
				{
					candidates.Add(top);
				}
			}

			List<int> result = candidates.ToList();

			for (int i = result.Count - 1; i > 0; i--)
			{
				int k = random.Next(i + 1);
				int tmp = result[k];
				result[k] = result[i];
				result[i] = tmp;
			}

			return result;
		}
		public Queue<int> GenerateRandomQueue(int count = 671, int min = 0, int max = 671)
		{
			if (max <= min || count < 0 || (count > max - min && max - min > 0))
			{
				throw new ArgumentOutOfRangeException("Range " + min + " to " + max +
						" (" + ((Int64)max - (Int64)min) + " values), or count " + count + " is illegal");
			}

			HashSet<int> candidates = new HashSet<int>();

			for (int top = max - count; top < max; top++)
			{
				if (!candidates.Add(random.Next(min, top + 1)))
				{
					candidates.Add(top);
				}
			}

			List<int> result = candidates.ToList();

			for (int i = result.Count - 1; i > 0; i--)
			{
				int k = random.Next(i + 1);
				int tmp = result[k];
				result[k] = result[i];
				result[i] = tmp;
			}

			Queue<int> q = new Queue<int>(result);
			return q;
		}
	}
}
