﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Data.Sqlite;
using System.Data;
using SQLite;

namespace Assets.Classes
{
	public static class DAOJoueurs
	{
		#region classes
		private class Joueurs
		{
			public int? id { get; set; }
			public string email { get; set; }
			public string mdp { get; set; }
			public string nom { get; set; }
			public string key { get; set; }
			public int score { get; set; }
			public int confirmed { get; set; }
			public void init(string email, string mdp, string nom, string key, int score = 0, int confirmed = 0)
			{
				this.id = null;
				this.email = email;
				this.mdp = mdp;
				this.nom = nom;
				this.key = key;
				this.score = score;
				this.confirmed = confirmed;
			}
		}
		#endregion

		static string Path = "D:\\db.db";

		#region methodes

		/// <returns>
		/// true : existe
		/// false : existe pas
		/// null : erreur
		/// </returns>
		public static bool? EmailExiste(string email)
		{
			try
			{
				SQLiteConnection cnx = new SQLiteConnection(Path);
				string query = "select email from Joueurs where email = ?;";
				var r = cnx.Query<Joueurs>(query, email);
				if (r.Capacity > 0 && r.Count > 0)
				{
					if (r[0].email == email)
						return true;
					else
						return false;
				}
				else
					return false;
			}
			catch (Exception e)
			{
				return null;
			}
		}

		/// <returns>
		/// -4 : exception
		/// -3 : non confirmé
		/// -2 : erreur mdp
		/// -1 : erreur email
		/// >0 : id
		/// </returns>
		public static int? Valide(string email, string mdp)
		{
			try
			{
				bool? x = EmailExiste(email);
				if (x == true)
				{
					SQLiteConnection cnx = new SQLiteConnection(Path);
					string query = "select id from Joueurs where email = ? and mdp = ?;";
					var r = cnx.Query<Joueurs>(query, email, mdp);
					if (r.Count > 0)
					{ 
						int? id = r[0].id;
						query = "select confirmed from Joueurs where id = ?;";
						r = cnx.Query<Joueurs>(query, id);
						if (r[0].confirmed == 1)
							return id;
						else
							return -3;
					}
					else
						return -2;
				}
				else if (x == false)
					return -1;
				else
					return -4;
			}
			catch (Exception e)
			{
				return -4;
			}
		}

		/// <returns>
		/// >0 : succes
		/// -1 : erreur 
		/// </returns>
		public static bool InsertJoueurs(string email, string mdp, string nom, string key, int score = 0)
		{
			try
			{
				SQLiteConnection cnx = new SQLiteConnection(Path);
				Joueurs x = new Joueurs();
				x.init(email, mdp, nom, key, score);
				cnx.Insert(x);
				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		/// <returns>
		/// true : succes
		/// false : erreur
		/// </returns>
		public static bool UpdateScore(int id, int score)
		{
			try
			{
				SQLiteConnection cnx = new SQLiteConnection(Path);
				string query = "Update Joueurs set score = score + ? where id = ?";
				cnx.Execute(query, score, id);
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		/// <returns>
		/// -1 : id inexistant
		/// -2 : erreur
		/// >0 : score
		/// </returns>
		public static int GetScore(int id)
		{
			try
			{
				SQLiteConnection cnx = new SQLiteConnection(Path);
				string query = "select score from Joueurs where id = ?;";
				var r = cnx.Query<Joueurs>(query, id);
				if (r.Capacity > 0 && r.Count > 0)
				{
					return r[0].score;
				}
				else
					return -1;
			}
			catch (Exception)
			{
				return -2;
			}
		}

		/// <returns>
		/// true : succes
		/// false : erreur
		/// </returns>
		public static bool UpdateMDP(int id, string mdp)
		{
			try
			{
				SQLiteConnection cnx = new SQLiteConnection(Path);
				string query = "Update Joueurs set mdp = ? where id = ?";
				cnx.Execute(query, mdp, id);
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		/// <returns>
		/// >0 : id
		/// -1 : not found
		/// -2 : erreur
		/// </returns>
		public static int GetID(string email)
		{
			try
			{
				SQLiteConnection cnx = new SQLiteConnection(Path);
				string query = "select id from Joueurs where email = ?;";
				var r = cnx.Query<Joueurs>(query, email);
				if (r.Capacity > 0 && r.Count > 0)
				{
					return (int)r[0].id;
				}
				return -1;
			}
			catch (Exception)
			{
				return -2;
			}
		}

		/// <returns>
		/// >0 : id
		/// -1 : not found
		/// -2 : erreur
		/// </returns>
		public static string GetNom(int id)
		{
			try
			{
				SQLiteConnection cnx = new SQLiteConnection(Path);
				string query = "select nom from Joueurs where id = ?;";
				var r = cnx.Query<Joueurs>(query, id);
				if (r.Capacity > 0 && r.Count > 0)
				{
					return r[0].nom;
				}
				return null;
			}
			catch (Exception)
			{
				return null;
			}
		}

		/// <returns>
		/// 1 : succes
		/// -1 : lien invalide
		/// -2 : erreur
		/// </returns>
		public static int Confirm(int id, string key)
		{
			try
			{
				SQLiteConnection cnx = new SQLiteConnection(Path);
				string query = "select key from Joueurs where id = ?;";
				var r = cnx.Query<Joueurs>(query, id);
				if (r.Capacity > 0 && r.Count > 0)
				{
					if (r[0].key == key)
					{
						query = "Update Joueurs set confirmed = 1 where id = ?";
						cnx.Execute(query, id);
						return 1;
					}
					else
						return -1;
				}
				return -1;
			}
			catch (Exception)
			{
				return -2;
			}
		}
		#endregion
	}
}